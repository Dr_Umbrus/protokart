﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathData : ScriptableObject
{

    
    public List<Vector2> points;
    public bool isClose;
    public bool autoSet;
}
