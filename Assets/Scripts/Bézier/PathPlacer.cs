﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPlacer : MonoBehaviour
{
    public float spacing = .1f;
    public float resolution = 1;
    public List<GameObject> spheres = new List<GameObject>();

    private void Start()
    {
        CreateSphere();
    }

    public void CreateSphere()
    {
        Vector2[] points = GetComponent<PathCreator>().path.CalculateEvenSpacePoints(spacing, resolution);
        foreach (Vector2 p in points)
        {
            GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            g.transform.position = p;
            g.transform.localScale = Vector3.one * spacing * .5f;

            spheres.Add(g);
        }
    }

    
}
