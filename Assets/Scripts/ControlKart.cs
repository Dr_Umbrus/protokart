﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControlKart : MonoBehaviour
{
    public float acceleration;
    public float speed;
    public float maxcceleration;
    public float turnPower;
    public float maxSpeed;
    public Rigidbody2D rb;
    bool accel = false;
    Vector2 movement;
    bool reverse = false;
    public ItemClass currentItem;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    public void FixedUpdate()
    {
        if (accel)
        {
            if (speed < maxcceleration)
            {
                if (speed < 0)
                {
                    speed += Time.deltaTime * acceleration * 2;
                }
                else
                {
                    speed += Time.deltaTime * acceleration;
                }
                
            }
            else
            {
                speed = maxcceleration;
            }
        }
        else if(!reverse)
        {
            if (speed > 0)
            {
                speed -= Time.deltaTime * acceleration * 2;
            }
            else
            {
                speed += Time.deltaTime * acceleration * 2 ;
            }

            if (Mathf.Abs(speed) < 1)
            {
                speed = 0;
            }
        }

        if (reverse)
        {
            if (speed > (-maxcceleration / 2))
            {
                speed -= Time.deltaTime * acceleration;
            }
        }

        if (rb.velocity.magnitude < maxSpeed)
        {
            rb.AddRelativeForce(new Vector2(0, speed));
        }
        rb.AddTorque(turnPower * -movement.x);

        
    }

    public void OnAccelerate()
    {
        accel = true;
        
    }

    public void OnAccelerate1()
    {
        accel = false;
    }

    public void OnTurn(InputValue value)
    {
        Vector2 potato = value.Get<Vector2>();
        

        if (potato.x<-0.3f || potato.x>0.3f)
        {
            movement = potato;
            
            
        }
        else
        {
            movement = Vector2.zero;
        }
    }

    public void OnReverse()
    {
        reverse = true;
    }

    public void OnReverse1()
    {
        reverse = false;
    }

    public void OnItemPress()
    {
        currentItem.UsingEffect();
    }
}
