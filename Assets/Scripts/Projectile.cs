﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed;
    public GameObject portail;

    private void Update()
    {
        transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject temp =Instantiate(portail, transform.position, Quaternion.identity);
        

    }
}
