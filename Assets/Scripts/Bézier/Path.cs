﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path
{
    [SerializeField]
    public List<Vector2> points;
    [SerializeField]
    public bool isClose;
    [SerializeField]
    public bool autoSet;

    public Path(Vector2 centre)
    {
        points = new List<Vector2>
        {
            centre+Vector2.left,
            centre+(Vector2.left+Vector2.up)*.5f,
            centre+(Vector2.right+Vector2.down)*.5f,
            centre+Vector2.right
        };
    }

    public Vector2 this[int i]
    {
        get
        {
            return points[i];
        }
    }

    public bool IsClose
    {
        get
        {
            return isClose;
        }
        set
        {
            if (isClose != value)
            {
                isClose = value;

                if (isClose)
                {
                    points.Add(points[points.Count - 1] * 2 - points[points.Count - 2]);
                    points.Add(points[0] * 2 - points[1]);

                    if (autoSet)
                    {
                        AutoSetAnchorPoints(0);
                        AutoSetAnchorPoints(points.Count - 3);
                    }
                }
                else
                {
                    points.RemoveRange(points.Count - 2, 2);
                    if (autoSet)
                    {
                        AutoSetSEControls();
                    }
                }
            }
        }
    }

    public bool AutosetControlPoints
    {
        get
        {
            return autoSet;
        }

        set
        {
            if (autoSet != value)
            {
                autoSet = value;
                if (autoSet)
                {
                    AutoSetAllControl();
                }
            }
        }
    }

    public int NumPoints
    {
        get
        {
            return points.Count;
        }
    }

   

    public int NumSegments
    {
        get
        {
            return points.Count/3;
        }
    }

    public void AddSegment(Vector2 anchorPos)
    {
        points.Add(points[points.Count - 1] * 2 - points[points.Count - 2]);
        points.Add(points[points.Count - 1] + anchorPos*.5f);
        points.Add(anchorPos);

        if (autoSet)
        {
            AutoSetAllAffectedControl(points.Count - 1);
        }
    }

    public void SplitSegment(Vector2 anchorPos, int segmentIndex)
    {
        points.InsertRange(segmentIndex * 3 + 2, new Vector2[]{
            Vector2.zero,
            anchorPos,
            Vector2.zero
        });

        if (autoSet)
        {
            AutoSetAllAffectedControl(segmentIndex * 3 + 3);
        }
        else
        {
            AutoSetAnchorPoints(segmentIndex * 3 + 3);
        }
    }

    public void DeleteSegment(int anchorIndex)
    {
        if(NumSegments>2 || (!isClose && NumSegments > 1))
        {
            if (anchorIndex == 0)
            {
                if (isClose)
                {
                    points[points.Count - 1] = points[2];
                }
                points.RemoveRange(0, 3);
            }
            else if (anchorIndex == points.Count - 1 && !isClose)
            {
                points.RemoveRange(anchorIndex - 2, 3);
            }
            else
            {
                points.RemoveRange(anchorIndex - 1, 3);
            }
        }
        
    }

    public Vector2[] GetPointsInSegment(int i)
    {
        return new Vector2[]
        {
            points[i*3],
            points[i*3+1],
            points[i*3+2],
            points[LoopIndex(i*3+3)]
        };
    }

    public void MovePoint(int i, Vector2 pos)
    {
        Vector2 deltaMove = pos - points[i];
        if(i%3==0 || !autoSet)
        {
            points[i] = pos;
        
        

        if (autoSet)
        {
            AutoSetAllAffectedControl(i);
        }
        else
        {
            if (i % 3 == 0)
            {
                if (i + 1 < points.Count || isClose)
                {
                    points[LoopIndex(i + 1)] += deltaMove;
                }
                if (i - 1 >= 0 || isClose)
                {
                    points[LoopIndex(i - 1)] += deltaMove;
                }

            }
            else
            {
                bool nextAnchor = (i + 1) % 3 == 0;
                int correspondIndex = (nextAnchor) ? i + 2 : i - 2;
                int anchorIndex = (nextAnchor) ? i + 1 : i - 1;

                if (correspondIndex >= 0 && correspondIndex < points.Count || isClose)
                {
                    float dst = (points[LoopIndex(anchorIndex)] - points[LoopIndex(correspondIndex)]).magnitude;
                    Vector2 dir = (points[LoopIndex(anchorIndex)] - pos).normalized;
                    points[LoopIndex(correspondIndex)] = points[LoopIndex(anchorIndex)] + dir * dst;
                }

            }
        }

        }
    }

    public Vector2[] CalculateEvenSpacePoints(float spacing, float resolution = 1)
    {
        List<Vector2> evenlySpace = new List<Vector2>();
        evenlySpace.Add(points[0]);
        Vector2 previousPoint = points[0];
        float dstSinceLast = 0;

        for (int segmentIndex=0; segmentIndex<NumSegments; segmentIndex++)
        {
            Vector2[] p = GetPointsInSegment(segmentIndex);
            float controlNetLegnth = Vector2.Distance(p[0], p[1]) + Vector2.Distance(p[1], p[2]) + Vector2.Distance(p[2], p[3]);
            float estimatedLength = Vector2.Distance(p[0], p[3]) + controlNetLegnth / 2;
            int div = Mathf.CeilToInt(estimatedLength * resolution * 10);
            float t = 0;
            while (t <= 1)
            {
                t += .1f/div;
                Vector2 pointOnCurve = Bezier.EvaluateCube(p[0], p[1], p[2], p[3], t);
                dstSinceLast += Vector2.Distance(previousPoint, pointOnCurve);

                while (dstSinceLast >= spacing)
                {
                    float overshoot = dstSinceLast - spacing;
                    Vector2 newEvenlySpace = pointOnCurve + (previousPoint - pointOnCurve).normalized * overshoot;
                    evenlySpace.Add(newEvenlySpace);
                    dstSinceLast = overshoot;
                    previousPoint = newEvenlySpace;
                }

                previousPoint = pointOnCurve;
            }
        }

        return evenlySpace.ToArray();
    }

    int LoopIndex(int i)
    {
        return (i + points.Count) % points.Count;
    }

    void AutoSetAllAffectedControl(int updatedIndex)
    {
        for (int i = updatedIndex-3; i < updatedIndex+3; i+=3)
        {
            if(i>=0 && i<points.Count || isClose)
            {
                AutoSetAnchorPoints(LoopIndex(i));
            }
        }
    }

    void AutoSetAllControl()
    {
        for (int i = 0; i < points.Count; i+=3)
        {
            AutoSetAnchorPoints(i);
        }

        AutoSetSEControls();
    }

    void AutoSetAnchorPoints(int anchorIndex)
    {
        Vector2 anchorPos = points[anchorIndex];
        Vector2 dir = Vector2.zero;
        float[] neighbourDist = new float[2];

        if(anchorIndex-3>=0 || isClose)
        {
            Vector2 offset = points[LoopIndex(anchorIndex - 3)] - anchorPos;
            dir += offset.normalized;
            neighbourDist[0] = offset.magnitude;
        }
        if (anchorIndex + 3 >= 0 || isClose)
        {
            Vector2 offset = points[LoopIndex(anchorIndex + 3)] - anchorPos;
            dir -= offset.normalized;
            neighbourDist[0] = offset.magnitude;
        }
        dir.Normalize();

        for (int i = 0; i < 2; i++)
        {
            int controlIndex = anchorIndex + i * 2 - 1;
            if(controlIndex>=0 && controlIndex<points.Count || isClose)
            {
                points[LoopIndex(controlIndex)] = anchorPos + dir * neighbourDist[i] * .5f;
            }
        }    
    }


    void AutoSetSEControls()
    {
        if (!isClose)
        {
            points[1] = (points[0] + points[2]) * .5f;
            points[points.Count - 2] = (points[points.Count - 1] + points[points.Count - 3]) * .5f;
        }
    }
}
