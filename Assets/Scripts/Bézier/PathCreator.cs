﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCreator : MonoBehaviour
{
    [HideInInspector]
    public Path path;

    public Color anchorCol = new Color(0.7f, 0, 0.8f, 1);
    public Color controlCol = Color.white;
    public Color segmentCol = Color.green;
    public Color selectedSeg = Color.red;
    public float anchorSize = 0.5f;
    public float controlSize = 0.35f;
    public bool displayControl = true;

    public void CreatePath()
    {
        path = new Path(transform.position);
    }
}
