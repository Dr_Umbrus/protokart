// GENERATED AUTOMATICALLY FROM 'Assets/Kart.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Kart : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Kart()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Kart"",
    ""maps"": [
        {
            ""name"": ""MainKart"",
            ""id"": ""6856be7f-67ff-4812-911f-6753bc5d6933"",
            ""actions"": [
                {
                    ""name"": ""Accelerate"",
                    ""type"": ""Button"",
                    ""id"": ""56a39296-f378-4fea-92ef-98e845366bc1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Accelerate1"",
                    ""type"": ""Button"",
                    ""id"": ""688162fb-cee7-4d40-9573-5ee7425ba624"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""Turn"",
                    ""type"": ""Value"",
                    ""id"": ""cc7b3f2d-2348-49ac-b03f-b5abe84960d7"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Reverse"",
                    ""type"": ""Button"",
                    ""id"": ""b9625357-67a1-4f92-9957-7081afdbdfb6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Reverse1"",
                    ""type"": ""Button"",
                    ""id"": ""39a0f3de-4a36-4510-aad3-b0b4e9d33972"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""ItemPress"",
                    ""type"": ""Button"",
                    ""id"": ""debc1f5e-d218-49d9-91e1-80d508973607"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""86dc2fd3-31fd-430c-8065-20c6ecec18f4"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Accelerate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fd08c202-4683-4290-ae0c-f743dbd017dd"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a39cc874-029d-4e51-afc6-5f8d584e7fb5"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Accelerate1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""41ae7621-f36e-4ea7-9438-9ee6df519058"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Reverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""86431be4-9f25-4fac-996c-299aca314467"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Reverse1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""01f340a3-c4b5-426e-895a-9bd4ce0552a2"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""ItemPress"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Controller"",
            ""bindingGroup"": ""Controller"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // MainKart
        m_MainKart = asset.FindActionMap("MainKart", throwIfNotFound: true);
        m_MainKart_Accelerate = m_MainKart.FindAction("Accelerate", throwIfNotFound: true);
        m_MainKart_Accelerate1 = m_MainKart.FindAction("Accelerate1", throwIfNotFound: true);
        m_MainKart_Turn = m_MainKart.FindAction("Turn", throwIfNotFound: true);
        m_MainKart_Reverse = m_MainKart.FindAction("Reverse", throwIfNotFound: true);
        m_MainKart_Reverse1 = m_MainKart.FindAction("Reverse1", throwIfNotFound: true);
        m_MainKart_ItemPress = m_MainKart.FindAction("ItemPress", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // MainKart
    private readonly InputActionMap m_MainKart;
    private IMainKartActions m_MainKartActionsCallbackInterface;
    private readonly InputAction m_MainKart_Accelerate;
    private readonly InputAction m_MainKart_Accelerate1;
    private readonly InputAction m_MainKart_Turn;
    private readonly InputAction m_MainKart_Reverse;
    private readonly InputAction m_MainKart_Reverse1;
    private readonly InputAction m_MainKart_ItemPress;
    public struct MainKartActions
    {
        private @Kart m_Wrapper;
        public MainKartActions(@Kart wrapper) { m_Wrapper = wrapper; }
        public InputAction @Accelerate => m_Wrapper.m_MainKart_Accelerate;
        public InputAction @Accelerate1 => m_Wrapper.m_MainKart_Accelerate1;
        public InputAction @Turn => m_Wrapper.m_MainKart_Turn;
        public InputAction @Reverse => m_Wrapper.m_MainKart_Reverse;
        public InputAction @Reverse1 => m_Wrapper.m_MainKart_Reverse1;
        public InputAction @ItemPress => m_Wrapper.m_MainKart_ItemPress;
        public InputActionMap Get() { return m_Wrapper.m_MainKart; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MainKartActions set) { return set.Get(); }
        public void SetCallbacks(IMainKartActions instance)
        {
            if (m_Wrapper.m_MainKartActionsCallbackInterface != null)
            {
                @Accelerate.started -= m_Wrapper.m_MainKartActionsCallbackInterface.OnAccelerate;
                @Accelerate.performed -= m_Wrapper.m_MainKartActionsCallbackInterface.OnAccelerate;
                @Accelerate.canceled -= m_Wrapper.m_MainKartActionsCallbackInterface.OnAccelerate;
                @Accelerate1.started -= m_Wrapper.m_MainKartActionsCallbackInterface.OnAccelerate1;
                @Accelerate1.performed -= m_Wrapper.m_MainKartActionsCallbackInterface.OnAccelerate1;
                @Accelerate1.canceled -= m_Wrapper.m_MainKartActionsCallbackInterface.OnAccelerate1;
                @Turn.started -= m_Wrapper.m_MainKartActionsCallbackInterface.OnTurn;
                @Turn.performed -= m_Wrapper.m_MainKartActionsCallbackInterface.OnTurn;
                @Turn.canceled -= m_Wrapper.m_MainKartActionsCallbackInterface.OnTurn;
                @Reverse.started -= m_Wrapper.m_MainKartActionsCallbackInterface.OnReverse;
                @Reverse.performed -= m_Wrapper.m_MainKartActionsCallbackInterface.OnReverse;
                @Reverse.canceled -= m_Wrapper.m_MainKartActionsCallbackInterface.OnReverse;
                @Reverse1.started -= m_Wrapper.m_MainKartActionsCallbackInterface.OnReverse1;
                @Reverse1.performed -= m_Wrapper.m_MainKartActionsCallbackInterface.OnReverse1;
                @Reverse1.canceled -= m_Wrapper.m_MainKartActionsCallbackInterface.OnReverse1;
                @ItemPress.started -= m_Wrapper.m_MainKartActionsCallbackInterface.OnItemPress;
                @ItemPress.performed -= m_Wrapper.m_MainKartActionsCallbackInterface.OnItemPress;
                @ItemPress.canceled -= m_Wrapper.m_MainKartActionsCallbackInterface.OnItemPress;
            }
            m_Wrapper.m_MainKartActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Accelerate.started += instance.OnAccelerate;
                @Accelerate.performed += instance.OnAccelerate;
                @Accelerate.canceled += instance.OnAccelerate;
                @Accelerate1.started += instance.OnAccelerate1;
                @Accelerate1.performed += instance.OnAccelerate1;
                @Accelerate1.canceled += instance.OnAccelerate1;
                @Turn.started += instance.OnTurn;
                @Turn.performed += instance.OnTurn;
                @Turn.canceled += instance.OnTurn;
                @Reverse.started += instance.OnReverse;
                @Reverse.performed += instance.OnReverse;
                @Reverse.canceled += instance.OnReverse;
                @Reverse1.started += instance.OnReverse1;
                @Reverse1.performed += instance.OnReverse1;
                @Reverse1.canceled += instance.OnReverse1;
                @ItemPress.started += instance.OnItemPress;
                @ItemPress.performed += instance.OnItemPress;
                @ItemPress.canceled += instance.OnItemPress;
            }
        }
    }
    public MainKartActions @MainKart => new MainKartActions(this);
    private int m_ControllerSchemeIndex = -1;
    public InputControlScheme ControllerScheme
    {
        get
        {
            if (m_ControllerSchemeIndex == -1) m_ControllerSchemeIndex = asset.FindControlSchemeIndex("Controller");
            return asset.controlSchemes[m_ControllerSchemeIndex];
        }
    }
    public interface IMainKartActions
    {
        void OnAccelerate(InputAction.CallbackContext context);
        void OnAccelerate1(InputAction.CallbackContext context);
        void OnTurn(InputAction.CallbackContext context);
        void OnReverse(InputAction.CallbackContext context);
        void OnReverse1(InputAction.CallbackContext context);
        void OnItemPress(InputAction.CallbackContext context);
    }
}
