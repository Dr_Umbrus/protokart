﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathCreator))]
public class PathEditor : Editor
{
    string[] names;
    public string circuitName;
    PathData[] datas;
    PathCreator creator;
    Path path;
    public int indexLoad = 0;

    const float segmentSelectThreshold = .1f;
    int selectedSegIndex = -1;

    

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUI.BeginChangeCheck();

        GUILayout.Label("CircuitName", EditorStyles.boldLabel);
        circuitName = EditorGUILayout.TextArea(circuitName);

        if (GUILayout.Button("CreateNew"))
        {
            Undo.RecordObject(creator, "new");
            creator.CreatePath();
            path = creator.path;
            Debug.Log(path.points.Count);
        }

        bool isClose= GUILayout.Toggle(path.IsClose, "loop");
        if (isClose != path.isClose)
        {
            Undo.RecordObject(creator, "Toggle close");
            path.IsClose= isClose;
        }


        bool autoSetControl = GUILayout.Toggle(path.AutosetControlPoints, "Auto");
        if (autoSetControl != path.AutosetControlPoints)
        {
            Undo.RecordObject(creator, "Toggle auto");
            path.AutosetControlPoints = autoSetControl;
        }

        //Le label pour la liste des scriptablecolor
        GUILayout.Label("CircuitList", EditorStyles.boldLabel);
        //L'int index permet de récupérer l'option sélectionnée dans le Popup. L'array options permet d'utiliser les noms des fichiers dans le popup.
        indexLoad = EditorGUILayout.Popup(indexLoad, names);

        if (GUILayout.Button("Save"))
        {
            PathData asset = ScriptableObject.CreateInstance<PathData>();
            asset.points = path.points;
            asset.isClose = path.isClose;
            asset.autoSet = path.autoSet;

            if (circuitName != null)
            {
                int already = -1;
                for (int i = 0; i < names.Length; i++)
                {
                    if (circuitName == names[i])
                    {
                        already = i;
                    }
                }

                if (already == -1)
                {
                    Debug.Log(asset.points.Count);
                    AssetDatabase.CreateAsset(asset, "Assets/Circuits/" + circuitName + ".asset");
                    AssetDatabase.SaveAssets();
                }
                else
                {
                    datas[already].points = path.points;
                    datas[already].isClose = path.isClose;
                    datas[already].autoSet = path.autoSet;
                    AssetDatabase.SaveAssets();
                }

                GetLoadData();

                for (int i = 0; i < names.Length; i++)
                {
                    if (circuitName == names[i])
                    {
                        if (datas[datas.Length - 1].points != null)
                        {
                            Debug.Log(datas[i].name);
                            Debug.Log(datas[i].points.Count);
                            Debug.Log("ok 3");
                            Debug.Log(path.points.Count);
                        }
                        else
                        {
                            Debug.Log(":'( 3");
                        }
                    }
                }
                
            }

            
        }

        if (GUILayout.Button("Load"))
        {
            if (indexLoad >= 0)
            {
                /*PathPlacer pp = FindObjectOfType<PathPlacer>();
                foreach (GameObject g in pp.spheres)
                {
                    Destroy(g);
                }*/

                path.points = datas[indexLoad].points;

                path.isClose = datas[indexLoad].isClose;
                path.autoSet = datas[indexLoad].autoSet;
                circuitName = names[indexLoad];

               // pp.CreateSphere();
            }
        }

        if (EditorGUI.EndChangeCheck())
        {
            SceneView.RepaintAll();
        }
    }

    /*public void Load(int i)
    {

       
        if(i>=0 && i < datas.Length)
        {
            indexLoad = i;
            PathPlacer pp = FindObjectOfType<PathPlacer>();
            foreach (GameObject g in pp.spheres)
            {
                Destroy(g);
            }

            path.points = datas[indexLoad].points;

            path.isClose = datas[indexLoad].isClose;
            path.autoSet = datas[indexLoad].autoSet;
            circuitName = names[indexLoad];

            pp.CreateSphere();
        }
    }*/

    void OnSceneGUI()
    {
        Input();
        Draw();
    }

    void Input()
    {
        Event guiEvent = Event.current;
        Vector2 mousePos = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition).origin;

        if(guiEvent.type == EventType.MouseDown && guiEvent.button==0 && guiEvent.shift)
        {
            if (selectedSegIndex != -1)
            {
                Undo.RecordObject(creator, "Split segment");
                path.SplitSegment(mousePos, selectedSegIndex);
            }
            else if(!path.isClose)
            {
                Undo.RecordObject(this, "Add segment");
                path.AddSegment(mousePos);
            }
            
        }

        if(guiEvent.type==EventType.MouseDown && guiEvent.button == 1)
        {
            float minDis = creator.controlSize/2;
            int closestIndex = -1;

            for (int i = 0; i < path.NumPoints; i+=3)
            {
                float dist = Vector2.Distance(mousePos, path[i]);

                if (dist < minDis)
                {
                    minDis = dist;
                    closestIndex = i;
                }
            }

            if (closestIndex != -1)
            {
                Undo.RecordObject(creator, "Delete segment");
                path.DeleteSegment(closestIndex);
            }
        }

        if (guiEvent.type == EventType.MouseMove)
        {
            float minDisToSeg = segmentSelectThreshold;
            int newSelectInd = -1;

            for (int i = 0; i < path.NumSegments; i++)
            {
                Vector2[] points = path.GetPointsInSegment(i);
                float dst = HandleUtility.DistancePointBezier(mousePos, points[0], points[3], points[1], points[2]);
                if (dst < minDisToSeg)
                {
                    minDisToSeg = dst;
                    newSelectInd = i;
                }
            }

            if(newSelectInd != selectedSegIndex)
            {
                selectedSegIndex = newSelectInd;
                HandleUtility.Repaint();
            }
        }


        HandleUtility.AddDefaultControl(0);
    }

    void GetLoadData()
    {
        string[] str = AssetDatabase.FindAssets("t:" + typeof(PathData).Name, new[] { "Assets/Circuits" });

        //On initialise l'array de scriptablecolor pour qu'il ait la même taille que le nombre qu'on a trouvé avec le FindAssets. Pareil pour options.
        datas = new PathData[str.Length];
        names = new string[str.Length];


        for (int i = 0; i < str.Length; i++)
        {
            //On récupère le chemin pour accéder à la scriptablecolor.
            string path = AssetDatabase.GUIDToAssetPath(str[i]);
            //On charge ensuite la ScriptableColor associée à ce chemin dans l'array.
            datas[i] = AssetDatabase.LoadAssetAtPath<PathData>(path);
            //Et on récupère le nom du fichier pour le placer dans options. /!\ ce nom n'est pas la même chose que "t:" + typeof(ScriptableColor).Name
            names[i] = datas[i].name;
        }
    }

    private void OnEnable()
    {
        creator = (PathCreator)target;
        if (creator.path == null)
        {
            creator.CreatePath();
        }

        path = creator.path;
        GetLoadData();
        creator.path.points = datas[indexLoad].points;
        creator.path.isClose = datas[indexLoad].isClose;
        creator.path.autoSet = datas[indexLoad].autoSet;
    }

    void Draw()
    {
        for (int i = 0; i < path.NumSegments; i++)
        {
            Vector2[] points = path.GetPointsInSegment(i);
            Handles.color = Color.black;
            Handles.DrawLine(points[1], points[0]);
            Handles.DrawLine(points[2], points[3]);
            Color segmentCol = (i == selectedSegIndex && Event.current.shift) ? creator.segmentCol : creator.selectedSeg;
            Handles.DrawBezier(points[0], points[3], points[1], points[2], segmentCol, null, 2);
        }

        Handles.color = Color.blue;
        for (int i = 0; i < path.NumPoints; i++)
        {
            if(i%3==0 || creator.displayControl)
            {
                Handles.color = (i % 3 == 0) ? creator.anchorCol : creator.controlCol;
                float handleSize = (i % 3 == 0) ? creator.anchorSize : creator.controlSize;
                Vector2 newPos = Handles.FreeMoveHandle(path[i], Quaternion.identity, .5f, Vector2.zero, Handles.CylinderHandleCap);
                if (path[i] != newPos)
                {
                    Undo.RecordObject(creator, "Move point");
                    path.MovePoint(i, newPos);
                }
            }
           
        }

        
    }
}
