﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MysteryBox : MonoBehaviour
{
    public ItemClass[] items;
    public int i;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<ControlKart>() != null)
        {

            Debug.Log("Touché la box");
            other.GetComponent<ControlKart>().currentItem = items[Random.Range(0, (items.Length))];
        }
        Destroy(gameObject);

        
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
