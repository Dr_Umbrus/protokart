﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Item
{
    public class ItemTest1 : ItemClass
    {

        public ControlKart ck;
        public Rigidbody Rb;
        private float originalAcc;
        public float acceleractionPlus = -3000;


        // Start is called before the first frame update
        /* void Start()
         {
             GameObject.FindGameObjectWithTag("Player").GetComponent < ControlKart > ();
         }*/

        void Awake()
        {
            ck = FindObjectOfType(typeof(ControlKart)) as ControlKart;
            ;

            originalAcc = ck.acceleration;
        }

        /// Utiliser effet de l'objet grâce au void input OnItemPress() 
        public override void UsingEffect()
        {

            Debug.Log("item1");

            ck.rb.AddRelativeForce(new Vector2(0, ck.speed*500f));
            Destroy(ck.currentItem);

        }

      
    }
}
